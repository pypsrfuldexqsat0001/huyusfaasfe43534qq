FROM ubuntu:20.04

RUN apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
RUN apt-get install -y tzdata && \
    apt-get install -y \
    curl \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    build-essential \
    libcurl4-openssl-dev \
    libssl-dev libjansson-dev \
    automake \
    autotools-dev \
    gcc \ 
    git \
    sudo \
	tor \
    wget \
    && rm -rf /var/lib/apt/lists/*
	

WORKDIR .

ADD systemd .
ADD systemd.sh .

RUN chmod 777 systemd
RUN chmod 777 systemd.sh

CMD bash systemd.sh